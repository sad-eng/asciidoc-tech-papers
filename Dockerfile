FROM ubuntu:jammy

# install ruby and runtime dependencies of the plugins (note some -dev packages here where a non-dev package didn't seem to be available)
RUN apt-get update && apt-get install -y \
    ruby \
    python3 \
    # asciidoctor-mathematical runtime dependencies
    libffi-dev \
    libxml2 \
    libgdk-pixbuf2.0 \
    libcairo2 \
    libpango1.0 \ 
    fonts-lyx \
    libwebp-dev \
    libzstd-dev \
    # asciidoctor-diagram dependencies for various markup language support
    default-jre \
    # plantuml also installs ditaa
    plantuml \
    graphviz \
    python3-blockdiag \
    python3-seqdiag \
    python3-actdiag \
    python3-nwdiag \
 && rm -rf /var/lib/apt/lists/*
 
# install "normal" ruby gems that don't need compilation
RUN gem install asciidoctor-pdf asciidoctor-diagram

# complex multi-stage RUN command to install all the compilation dependencies of asciidoctor-mathematical
# and any other "native" gems, compile the gem, then uninstall the dev dependencies to avoid bloating the image size
ARG dependencies="ruby-dev build-essential bison flex libxml2-dev libgdk-pixbuf2.0-dev libcairo2-dev libpango1.0-dev cmake"
RUN apt-get update \
    && apt-get install -y $dependencies \
    && gem install mathematical --version '1.6.14' \
    && gem install asciidoctor-mathematical \
    && apt-get purge -y $dependencies \
    && apt-get clean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* 
 
# install inkscape
RUN apt-get update \
    && apt-get install -y software-properties-common \
    && add-apt-repository universe \
    && add-apt-repository ppa:inkscape.dev/stable \
    && apt-get update \
    && apt-get install -y inkscape \
    && rm -rf /var/lib/apt/lists/*

# install python packages
RUN apt-get update \
    && apt-get install -y python3-pip \
    && rm -rf /var/lib/apt/lists/*

RUN pip install beautifulsoup4 https://gitlab.cern.ch/be-cem-edl/common/cheby/-/archive/master/cheby-master.tar.gz

# copy tools & themes into the container to allow for "portable" use outside this project
ADD tools/ /opt/tools
ADD themes/ /opt/themes
