#!/bin/bash

DOCKER_IMAGE=${DOCKER_IMAGE:-registry.gitlab.com/sad-eng/asciidoc-tech-papers:latest}

if command -v podman &> /dev/null
then
    engine=podman
elif command -v docker &> /dev/null
then
    engine=docker
else
    echo "Could not find either podman or docker commands, giving up"
    exit 1
fi

echo "Starting container $DOCKER_IMAGE..."
container=$($engine run -dt -v "$(pwd)":/root --user "$(id -u):$(id -g)" "$DOCKER_IMAGE")

$engine exec -w /root "$container" python3 /opt/tools/build_docs.py "$@"

echo "Stopping container..."
$engine stop $container
