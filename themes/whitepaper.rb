class PDFConverterCustomTitlePage < (Asciidoctor::Converter.for 'pdf')
  register_for 'pdf'


  def ink_title_page doc
    move_cursor_to page_height * 0.75
    theme_font :title_page do
      stroke_horizontal_rule '2967B2', line_width: 1.5, line_style: :double
      move_down 10
      doctitle = doc.doctitle partition: true
      theme_font :title_page_title do
        ink_prose doctitle.main, align: :center, color: theme.base_font_color, line_height: 1, margin: 0
      end
      if (subtitle = doctitle.subtitle)
        theme_font :title_page_subtitle do
          move_down 10
          ink_prose subtitle, align: :center, margin: 0
          move_down 10
        end
      end
      stroke_horizontal_rule '2967B2', line_width: 1.5, line_style: :double
      move_cursor_to page_height * 0.5
      convert ::Asciidoctor::Block.new doc, :image,
        content_model: :empty,
        attributes: { 'target' => 'sample-logo.jpg', 'pdfwidth' => '1.5in', 'align' => 'center' },
        pinned: true
    end
  end

  def convert_section sect, _opts = {}
  if (sectname = sect.sectname) == 'abstract'
    # HACK: cheat a bit to hide this section from TOC; TOC should filter these sections
    sect.context = :open
    return convert_abstract sect
  elsif (index_section = sectname == 'index') && @index.empty?
    sect.remove
    return
  end

  title = sect.numbered_title formal: true
  sep = (sect.attr 'separator') || (sect.document.attr 'title-separator') || ''
  if !sep.empty? && (title.include? (sep = %(#{sep} )))
    title, _, subtitle = title.rpartition sep
    title = %(#{title}\n<em class="subtitle">#{subtitle}</em>)
  end
  if title.include? ". "
    title[". "] = " "
  end
  hlevel = sect.level.next
  text_align = (@theme[%(heading_h#{hlevel}_text_align)] || @theme.heading_text_align || @base_text_align).to_sym
  chapterlike = !(part = sectname == 'part') && (sectname == 'chapter' || (sect.document.doctype == 'book' && sect.level == 1))
  hidden = sect.option? 'notitle'
  hopts = { align: text_align, level: hlevel, part: part, chapterlike: chapterlike, outdent: !(part || chapterlike) }
  if part
    if @theme.heading_part_break_before == 'always'
      started_new = true
      start_new_part sect
    end
  elsif chapterlike
    if (@theme.heading_chapter_break_before == 'always' &&
        !(@theme.heading_part_break_after == 'avoid' && sect.first_section_of_part?)) ||
        (@theme.heading_part_break_after == 'always' && sect.first_section_of_part?)
      started_new = true
      start_new_chapter sect
    end
  end
  arrange_heading sect, title, hopts unless hidden || started_new || at_page_top? || sect.empty?
  # QUESTION: should we store pdf-page-start, pdf-anchor & pdf-destination in internal map?
  sect.set_attr 'pdf-page-start', (start_pgnum = page_number)
  # QUESTION: should we just assign the section this generated id?
  # NOTE: section must have pdf-anchor in order to be listed in the TOC
  sect.set_attr 'pdf-anchor', (sect_anchor = derive_anchor_from_id sect.id, %(#{start_pgnum}-#{y.ceil}))
  add_dest_for_block sect, id: sect_anchor, y: (at_page_top? ? page_height : nil)
  theme_font :heading, level: hlevel do
    if part
      ink_part_title sect, title, hopts
    elsif chapterlike
      ink_chapter_title sect, title, hopts
    else
      ink_general_heading sect, title, hopts
    end
  end unless hidden

  if index_section
    outdent_section { convert_index_section sect }
  else
    traverse sect
  end
  outdent_section { ink_footnotes sect } if chapterlike
  sect.set_attr 'pdf-page-end', page_number
  end
end
