import argparse
import subprocess
import logging
from pathlib import Path

ROOT = Path(__file__).parent.parent
def build_docs(files, theme):
    logger = logging.getLogger(__name__)
    for file in files:
        logger.info(f'Building {file}')
        args = ['asciidoctor-pdf', '--theme', f'{theme}', '-a', f'pdf-themesdir={ROOT}/themes', f'{file}',
                f'-r', f'{ROOT}/themes/{theme}.rb', '-r', 'asciidoctor-mathematical', '-r', 'asciidoctor-diagram']
        
        logger.debug(f'Build Command: {" ".join(args)}')        
        p = subprocess.run(args, capture_output=True)
        stdout, stderr, ret_code = p.stdout.decode(), p.stderr.decode(), p.returncode        
        if ret_code == 0:
            logger.debug(f'Build Output {file}: {stdout}{stderr}')
        else:
            logger.error(f'Error building {file}: {stdout}{stderr}')
        
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('files', nargs='+')
    parser.add_argument('-l', '--log-level', choices=('ERROR', 'WARNING', 'INFO', 'DEBUG'), default='INFO')
    parser.add_argument('-t', '--theme', default='whitepaper',
                        help='The name of a theme in the /themes directory to use')
    args = parser.parse_args()
    logging.basicConfig(level=getattr(logging, args.log_level))
    build_docs(args.files, args.theme)
