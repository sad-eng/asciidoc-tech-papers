import argparse
import subprocess
import logging

logger=logging.getLogger()
logger.setLevel(logging.DEBUG)

def extract_svg(pdfname):
    page=1
    success = True
    basepdfname = pdfname.replace(".pdf","")

    # Without knowing how many pages are in the PDF, attempt to extract pages in order until we fail
    while(True): 
        logger.error(".")
        command = f"inkscape --pdf-page={page} --pdf-poppler -o {basepdfname}_p{page}.svg {basepdfname}.pdf"
        logger.debug(f" Executing: {command}")
        try:
            output = subprocess.check_output(command, stderr=subprocess.STDOUT, shell=True)
        except subprocess.CalledProcessError as e:
            logger.debug(f"Exeption: {e}")
            break
        
        output = output.decode("utf-8")
        logger.debug(f"output: {output}")
        if("Bad page" in output):
            break
        page += 1
    command = f"rm {basepdfname}_p{page}.svg"
    subprocess.check_output(command, shell=True)       # delete the last attempted SVD file as it's invalid 


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    args = parser.parse_args()
    extract_svg(args.filename)