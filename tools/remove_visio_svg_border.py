import argparse

from bs4 import BeautifulSoup


def main(file):
    with open(file) as f:
        svg = f.read()
    soup = BeautifulSoup(svg, 'xml')
    surface1 = soup.find(id='surface1')
    surface1.find('path')['style'] = surface1.find("path")['style'].replace('opacity:1', 'opacity:0')
    f = open(file,"w")
    f.write(soup.prettify())
    f.close()


if __name__ == "__main__":
    parser=argparse.ArgumentParser()
    parser.add_argument('filename')
    args=parser.parse_args()
    main(args.filename)


